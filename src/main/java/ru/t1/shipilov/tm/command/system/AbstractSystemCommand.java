package ru.t1.shipilov.tm.command.system;

import ru.t1.shipilov.tm.api.service.ICommandService;
import ru.t1.shipilov.tm.command.AbstractCommand;
import ru.t1.shipilov.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
