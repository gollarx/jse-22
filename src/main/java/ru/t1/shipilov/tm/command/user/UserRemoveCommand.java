package ru.t1.shipilov.tm.command.user;

import ru.t1.shipilov.tm.enumerated.Role;
import ru.t1.shipilov.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractUserCommand {

    private final String NAME = "user-remove";

    private final String DESCRIPTION = "User remove.";

    @Override
    public void execute() {
        System.out.println("[USER REMOVE]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        getUserService().removeByLogin(login);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
