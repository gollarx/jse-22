package ru.t1.shipilov.tm.command.task;

import ru.t1.shipilov.tm.model.Task;
import ru.t1.shipilov.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    private final String NAME = "task-show-by-id";

    private final String DESCRIPTION = "Show task by Id.";

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = getTaskService().findOneById(userId, id);
        showTask(task);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
