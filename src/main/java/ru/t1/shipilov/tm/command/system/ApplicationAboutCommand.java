package ru.t1.shipilov.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    private final String NAME = "about";

    private final String DESCRIPTION = "Show developer info.";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Alexey Shipilov");
        System.out.println("E-mail: gollarx@gmail.com");
    }

    @Override
    public String getArgument() {
        return "-a";
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
