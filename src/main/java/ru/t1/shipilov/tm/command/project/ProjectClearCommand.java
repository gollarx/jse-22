package ru.t1.shipilov.tm.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {

    private final String NAME = "project-clear";

    private final String DESCRIPTION = "Delete all projects.";

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[CLEAR PROJECTS]");
        getProjectService().clear(userId);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
